package main

import ( "fmt"
		 "net/http"
		 "time" 
		 "log")

func main() {

StartServer()

// Essa linha deve ser executada sem alteração
// da função StartServer	
log.Println("[INFO] Servidor no ar!")
}

func cebolas(w http.ResponseWriter, r *http.Request) {

url := fmt.Sprintf("%v %v", r.Method, r.URL)
w.Write([]byte(url))
}

func StartServer() {

duration, _ := time.ParseDuration("1000ns")

server := &http.Server{
Addr       : "172.22.51.159:8082",
IdleTimeout: duration, 
}

http.HandleFunc("/", cebolas)

log.Print(server.ListenAndServe())
}